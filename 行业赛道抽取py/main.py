import logging

import pandas as pd

from config import DEMO_CSV_PATH, OUTPUT_CSV_PATH, loggingConfig, LOG_FILE_NAME
from data_process.data_process import saidao_process
from model.model import compound
import re
import pandas as pd
import pdfplumber
import requests
from collections import Counter
from io import BytesIO
import datetime

# configure log file
logging.basicConfig(**loggingConfig)
logger = logging.getLogger(LOG_FILE_NAME)


def demo_func():
    # read data from database, 注释写中文也可以
    # read file from input directory
    #input_csv_data = pd.read_csv(DEMO_CSV_PATH)

    currency = ['人民币', '美元', '欧元', '日元', '港币', '英镑']
    areawords = ['美国', '全球', '中国', '我国', '美国']
    industry_list= pd.read_excel("/Users/chenxj00/Desktop/Demo-Project/input/industry1.xlsx",sheet_name=0).fillna('')
    root = pd.read_csv('/Users/chenxj00/Documents/长江证券/pro2行业赛道/研报191201_200717.csv')['ATTACHMENT'].dropna()
    root_id = pd.read_csv('/Users/chenxj00/Documents/长江证券/pro2行业赛道/研报191201_200717.csv')['NEWS_ID'].dropna()
    root_clean = [re.findall('"([^"]+)"', i) for i in root]
    totalsaidao_set = saidao_process(industry_list)
    temp_detail_track=industry_list['细分产品'].values.tolist()
    detail_track = list()
    for i in range(len(temp_detail_track)):
        detail_track += temp_detail_track[i].split('、')
    detail_track = [i for i in detail_track if i != '']
    result_total = compound(root_clean[2000:14000], totalsaidao_set, currency, areawords, root_id,detail_track,industry_list)
    result_mid=result_total[1]
    result_final = result_total[0]
    result_mid2=result_total[2]
    result_mid.to_csv('/Users/chenxj00/Desktop/Demo-Project/output/result_mid_total2.csv')
    result_final.to_csv('/Users/chenxj00/Desktop/Demo-Project/output/result_final_total2.csv')
    result_mid2.to_csv('/Users/chenxj00/Desktop/Demo-Project/output/result_med_total2.csv')


if __name__ == "__main__":
    # 调试可以写log，可以看到log/demo_project.log文件被写入
    # 也可以print，但最重要的提示信息，如运行时间、运行结果、错误信息需要写log
    logger.info('run demo main')
    demo_func()
