import pandas as pd
import logging
from config import LOG_FILE_NAME

logger = logging.getLogger(LOG_FILE_NAME)

import pdfplumber
import requests
from io import BytesIO



def getpdf(url):
    url = url
    rq = requests.get(url)
    pdf = pdfplumber.load(BytesIO(rq.content))
    full_text = ''
    for page in pdf.pages:
        text = page.extract_text()
        if text is None:
            full_text += ''
        else:
            full_text += text
    return(full_text)