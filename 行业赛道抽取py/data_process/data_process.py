import logging
from config import LOG_FILE_NAME

logger = logging.getLogger(LOG_FILE_NAME)
import re


def clean_data(full_text):
    full_text_clean = ''.join(re.findall('[^ [/t/r/n/f/v/\n/【】]', full_text))
    full_text_split = re.split(r'[！。]', full_text_clean)
    full_text_split_refine = [i for i in full_text_split if len(i) < 150]
    return (full_text_split_refine)


def saidao_process(industry_list):
    saidao = industry_list['赛道'].values.tolist()
    saidaosyn = industry_list['赛道同义词'].values.tolist()
    totalsaidao = saidao
    for i in range(len(saidaosyn)):
        totalsaidao[i] = (saidao[i] + ',' + saidaosyn[i]).split(',')
    totalsaidao_set = list()
    for i in range(len(totalsaidao)):
        for j in range(len(totalsaidao[i])):
            totalsaidao_set = totalsaidao_set + [(totalsaidao[i][j]).replace(' ', '')]
    totalsaidao_set = [i for i in totalsaidao_set if (len(str(i)) != 0)]
    return (totalsaidao_set)


def extract_sentence_rule(totalsaidao_set, currency, areawords):
    joinindustry = '[' + '|'.join(totalsaidao_set) + '].{0,2}?[市场|规模|行业].{0,4}?[收入|营收]{0,1}.{0,2}?'
    joincurrency = '[0-9]+[,\d]*[.\d]*.{1,2}?[' + '|'.join(currency) + ']'
    joinyear = '[0-9]{3,4}[年].{0,2}?'
    joinarea = '[' + '|'.join(areawords) + '].{0,2}'
    permutation_list = [joinindustry, joinarea, joinyear]
    permutation_rule = list()
    for i in range(len(permutation_list)):
        for j in range(len(permutation_list)):
            for k in range(len(permutation_list)):
                if (i != j) & (j != k) & (i != k):
                    permutation_rule += (
                    [permutation_list[i] + permutation_list[j] + permutation_list[k] + joincurrency])
    joinindustry_start = '[' + '|'.join(totalsaidao_set) + '].{0,2}[市场|行业].{0,2}'
    joinindustry_end = '[市场|规模|行业|收入|营收]'
    permutation_list1 = [joinarea, joinindustry_start, joinyear]
    for i in range(len(permutation_list1)):
        for j in range(len(permutation_list1)):
            for k in range(len(permutation_list1)):
                if (i != j) & (j != k) & (i != k):
                    permutation_rule += ([permutation_list1[i] + permutation_list1[j] + permutation_list1[
                        k] + joincurrency + joinindustry_end])
    return(permutation_rule)

def extract_sentence1(full_text_split_refine,permutation_rule):
    contain = list()
    for i in range(len(full_text_split_refine)):
        ifmatch = list()
        for k in range(len(permutation_rule)):
            temp = full_text_split_refine[i]
            ifmatch += re.findall(permutation_rule[k], temp)
        contain += [ifmatch]

    ifcontain_first = contain.copy()
    for j in range(len(ifcontain_first)):
        if len(ifcontain_first[j]) > 0:
            ifcontain_first[j] = True
        else:
            ifcontain_first[j] = False

    ifcontain_first_posi = [i for i, a in enumerate(ifcontain_first) if a == True]
    text_first = [full_text_split_refine[i] for i in ifcontain_first_posi]
    return (text_first, ifcontain_first_posi)

def extract_year_rule(totalsaidao_set, currency, areawords):
    joinindustry = '[' + '|'.join(totalsaidao_set) + '].{0,2}?[市场|规模|行业].{0,4}?[收入|营收]{0,1}.{0,2}?'
    joincurrency = '[0-9]+[,\d]*[.\d]*.{1,2}?[' + '|'.join(currency) + ']'
    joinyear = '([0-9]{3,4}[年]).{0,2}?'
    joinarea = '[' + '|'.join(areawords) + '].{0,2}'
    permutation_list = [joinindustry, joinarea, joinyear]
    permutation_rule = list()
    for i in range(len(permutation_list)):
        for j in range(len(permutation_list)):
            for k in range(len(permutation_list)):
                if (i != j) & (j != k) & (i != k):
                    permutation_rule += (
                        [permutation_list[i] + permutation_list[j] + permutation_list[k] + joincurrency])
    joinindustry_start = '[' + '|'.join(totalsaidao_set) + '].{0,2}[市场|行业].{0,2}'
    joinindustry_end = '[市场|规模|行业|收入|营收]'
    permutation_list1 = [joinarea, joinindustry_start, joinyear]
    for i in range(len(permutation_list1)):
        for j in range(len(permutation_list1)):
            for k in range(len(permutation_list1)):
                if (i != j) & (j != k) & (i != k):
                    permutation_rule += ([permutation_list1[i] + permutation_list1[j] + permutation_list1[
                        k] + joincurrency + joinindustry_end])
    return(permutation_rule)
def extract(x,permutation_rule ):
    temp = list()
    for h in range(len(permutation_rule)):
        temp += re.findall(permutation_rule[h], x)
    extract = list(set(temp))
    return (extract)

def scale_number_rule(totalsaidao_set, currency, areawords):
    joinindustry = '[' + '|'.join(totalsaidao_set) + '].{0,2}?[市场|规模|行业].{0,4}?[收入|营收]{0,1}.{0,2}?'
    joincurrency = '([0-9]+[,\d]*?[.\d]*).{1,2}?[' + '|'.join(currency) + ']'
    joinyear = '[0-9]{3,4}[年].{0,2}?'
    joinarea = '[' + '|'.join(areawords) + '].{0,2}'
    permutation_list = [joinindustry, joinarea, joinyear]
    permutation_rule = list()
    for i in range(len(permutation_list)):
        for j in range(len(permutation_list)):
            for k in range(len(permutation_list)):
                if (i != j) & (j != k) & (i != k):
                    permutation_rule += (
                        [permutation_list[i] + permutation_list[j] + permutation_list[k] + joincurrency])
    joinindustry_start = '[' + '|'.join(totalsaidao_set) + '].{0,2}[市场|行业].{0,2}'
    joinindustry_end = '[市场|规模|行业|收入|营收]'
    permutation_list1 = [joinarea, joinindustry_start, joinyear]
    for i in range(len(permutation_list1)):
        for j in range(len(permutation_list1)):
            for k in range(len(permutation_list1)):
                if (i != j) & (j != k) & (i != k):
                    permutation_rule += ([permutation_list1[i] + permutation_list1[j] + permutation_list1[
                        k] + joincurrency + joinindustry_end])
    return(permutation_rule)


def units_rule(totalsaidao_set, currency, areawords):
    joinindustry = '[' + '|'.join(totalsaidao_set) + '].{0,2}[市场|规模|行业].{0,4}[收入|营收]{0,1}.{0,2}?'
    joincurrency = '[0-9]+[.]?[0-9]?(.{0,2}?)[' + '|'.join(currency) + ']'
    joinyear = '[0-9]{2,4}[年].{0,2}'
    joinarea = '[' + '|'.join(areawords) + '].{0,2}'

    permutation_list = [joinindustry, joinarea, joinyear]
    permutation_rule = list()
    for i in range(len(permutation_list)):
        for j in range(len(permutation_list)):
            for k in range(len(permutation_list)):
                if (i != j) & (j != k) & (i != k):
                    permutation_rule += (
                        [permutation_list[i] + permutation_list[j] + permutation_list[k] + joincurrency])
    joinindustry_start = '[' + '|'.join(totalsaidao_set) + '].{0,2}[市场|行业].{0,2}'
    joinindustry_end = '[市场|规模|行业|收入|营收]'
    permutation_list1 = [joinarea, joinindustry_start, joinyear]
    for i in range(len(permutation_list1)):
        for j in range(len(permutation_list1)):
            for k in range(len(permutation_list1)):
                if (i != j) & (j != k) & (i != k):
                    permutation_rule += ([permutation_list1[i] + permutation_list1[j] + permutation_list1[
                        k] + joincurrency + joinindustry_end])
    return(permutation_rule)


def area_rule(totalsaidao_set, currency, areawords):
    joinindustry = '[' + '|'.join(totalsaidao_set) + '].{0,2}[市场|规模|行业].{0,4}[收入|营收]{0,1}.{0,2}?'
    joincurrency = '[0-9]+[,\d]*?[.\d+]*?.{0,2}[' + '|'.join(currency) + ']'
    joinyear = '[0-9]{2,4}[年].{0,2}'
    joinarea = '(' + '|'.join(areawords) + ').{0,2}'
    permutation_list = [joinindustry, joinarea, joinyear]
    permutation_rule = list()
    for i in range(len(permutation_list)):
        for j in range(len(permutation_list)):
            for k in range(len(permutation_list)):
                if (i != j) & (j != k) & (i != k):
                    permutation_rule += (
                        [permutation_list[i] + permutation_list[j] + permutation_list[k] + joincurrency])
    joinindustry_start = '[' + '|'.join(totalsaidao_set) + '].{0,2}[市场|行业].{0,2}'
    joinindustry_end = '[市场|规模|行业|收入|营收]'
    permutation_list1 = [joinarea, joinindustry_start, joinyear]
    for i in range(len(permutation_list1)):
        for j in range(len(permutation_list1)):
            for k in range(len(permutation_list1)):
                if (i != j) & (j != k) & (i != k):
                    permutation_rule += ([permutation_list1[i] + permutation_list1[j] + permutation_list1[
                        k] + joincurrency + joinindustry_end])
    return(permutation_rule)


def saidao_rule(totalsaidao_set, currency, areawords):
    joinindustry = '(' + '|'.join(totalsaidao_set) + ').{0,2}[市场|规模|行业].{0,4}[收入|营收]{0,1}.{0,2}?'
    joincurrency = '[0-9]+?[.]*?[0-9]*?.{0,2}[' + '|'.join(currency) + ']'
    joinyear = '[0-9]{2,4}[年].{0,2}'
    joinarea = '[' + '|'.join(areawords) + '].{0,2}'
    permutation_list = [joinindustry, joinarea, joinyear]
    permutation_rule = list()
    for i in range(len(permutation_list)):
        for j in range(len(permutation_list)):
            for k in range(len(permutation_list)):
                if (i != j) & (j != k) & (i != k):
                    permutation_rule += (
                        [permutation_list[i] + permutation_list[j] + permutation_list[k] + joincurrency])
    joinindustry_start = '[' + '|'.join(totalsaidao_set) + '].{0,2}[市场|行业].{0,2}'
    joinindustry_end = '[市场|规模|行业|收入|营收]'
    permutation_list1 = [joinarea, joinindustry_start, joinyear]
    for i in range(len(permutation_list1)):
        for j in range(len(permutation_list1)):
            for k in range(len(permutation_list1)):
                if (i != j) & (j != k) & (i != k):
                    permutation_rule += ([permutation_list1[i] + permutation_list1[j] + permutation_list1[
                        k] + joincurrency + joinindustry_end])
    return(permutation_rule)

def joinscale_rule(totalsaidao_set, currency, areawords):
    joinindustry = '(' + '|'.join(totalsaidao_set) + ').{0,2}[市场|规模|行业].{0,2}[收入|营收]{0,1}.{0,2}?'
    joincurrency = '[0-9]+?[.]*?[0-9]*?.{0,2}[' + '|'.join(currency) + ']'
    joinyear = '[0-9]{2,4}[年].{0,2}'
    joinarea = '[' + '|'.join(areawords) + '].{0,2}'
    permutation_list = [joinindustry, joinarea, joinyear]
    permutation_rule = list()
    for i in range(len(permutation_list)):
        for j in range(len(permutation_list)):
            for k in range(len(permutation_list)):
                if (i != j) & (j != k) & (i != k):
                    permutation_rule += (
                    [permutation_list[i] + permutation_list[j] + permutation_list[k] + joincurrency])
    joinindustry_start = '[' + '|'.join(totalsaidao_set) + '].{0,2}[市场|行业].{0,2}'
    joinindustry_end = '[市场|规模|行业|收入|营收]'
    permutation_list1 = [joinarea, joinindustry_start, joinyear]
    for i in range(len(permutation_list1)):
        for j in range(len(permutation_list1)):
            for k in range(len(permutation_list1)):
                if (i != j) & (j != k) & (i != k):
                    permutation_rule += ([permutation_list1[i] + permutation_list1[j] + permutation_list1[
                        k] + joincurrency + joinindustry_end])
    return(permutation_rule)


def currency_extract_rule(totalsaidao_set, currency, areawords):
    joinindustry = '[' + '|'.join(totalsaidao_set) + '].{0,2}[市场|规模|行业].{0,4}[收入|营收]{0,1}.{0,2}'
    joincurrency = '[0-9]+?[.]*?[0-9]*?.{0,2}(' + '|'.join(currency) + ')'
    joinyear = '[0-9]{2,4}[年].{0,2}'
    joinarea = '[' + '|'.join(areawords) + '].{0,2}'

    permutation_list = [joinindustry, joinarea, joinyear]
    permutation_rule = list()
    for i in range(len(permutation_list)):
        for j in range(len(permutation_list)):
            for k in range(len(permutation_list)):
                if (i != j) & (j != k) & (i != k):
                    permutation_rule += (
                        [permutation_list[i] + permutation_list[j] + permutation_list[k] + joincurrency])
    joinindustry_start = '[' + '|'.join(totalsaidao_set) + '].{0,2}[市场|行业].{0,2}'
    joinindustry_end = '[市场|规模|行业|收入|营收]'
    permutation_list1 = [joinarea, joinindustry_start, joinyear]
    for i in range(len(permutation_list1)):
        for j in range(len(permutation_list1)):
            for k in range(len(permutation_list1)):
                if (i != j) & (j != k) & (i != k):
                    permutation_rule += ([permutation_list1[i] + permutation_list1[j] + permutation_list1[
                        k] + joincurrency + joinindustry_end])
    return(permutation_rule)

def currency_extract_rule1(totalsaidao_set, areawords):
    joinindustry = '[' + '|'.join(totalsaidao_set) + '].{0,2}[市场|规模|行业].{0,4}[收入|营收]{0,1}.{0,2}'
    joincurrency = '[0-9]+?[.]*?[0-9]*?.{0,2}(元)'
    joinyear = '[0-9]{2,4}[年].{0,2}'
    joinarea = '[' + '|'.join(areawords) + '].{0,2}'

    permutation_list = [joinindustry, joinarea, joinyear]
    permutation_rule = list()
    for i in range(len(permutation_list)):
        for j in range(len(permutation_list)):
            for k in range(len(permutation_list)):
                if (i != j) & (j != k) & (i != k):
                    permutation_rule += (
                        [permutation_list[i] + permutation_list[j] + permutation_list[k] + joincurrency])
    joinindustry_start = '[' + '|'.join(totalsaidao_set) + '].{0,2}[市场|行业].{0,2}'
    joinindustry_end = '[市场|规模|行业|收入|营收]'
    permutation_list1 = [joinarea, joinindustry_start, joinyear]
    for i in range(len(permutation_list1)):
        for j in range(len(permutation_list1)):
            for k in range(len(permutation_list1)):
                if (i != j) & (j != k) & (i != k):
                    permutation_rule += ([permutation_list1[i] + permutation_list1[j] + permutation_list1[
                        k] + joincurrency + joinindustry_end])
    return(permutation_rule)

