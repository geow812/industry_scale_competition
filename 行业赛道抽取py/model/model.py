# All models here: import statsmodel, scikit-learn etc.
import pandas as pd
import logging
from config import LOG_FILE_NAME

from data_connect.database_connect import getpdf
from data_process.data_process import clean_data,extract_sentence1,extract,saidao_rule,currency_extract_rule,extract_year_rule,extract_sentence_rule,scale_number_rule,area_rule,units_rule,currency_extract_rule1
import datetime
import re
import time

logger = logging.getLogger(LOG_FILE_NAME)
def compound(root_clean,totalsaidao_set,currency,areawords,root_id,detail_track,industry_list):
    re_df=pd.DataFrame(columns=['行业赛道','细分产品','年份','行业规模','地区','币种','研报ID','时间戳','研报中位置','句子原文'])
    blacklist=['宠物疫苗']
    extractsentencerule_saidao = extract_sentence_rule(totalsaidao_set, currency, areawords)
    extractsentencerule_track = extract_sentence_rule(detail_track, currency, areawords)
    saidaorule = saidao_rule(totalsaidao_set, currency, areawords)
    detailtrackrule = saidao_rule(detail_track, currency, areawords)
    extractyearrule = extract_year_rule(totalsaidao_set, currency, areawords)
    scalenumberrule = scale_number_rule(totalsaidao_set, currency, areawords)
    arearule = area_rule(totalsaidao_set, currency, areawords)
    unitsrule = units_rule(totalsaidao_set, currency, areawords)
    currencyextractrule = currency_extract_rule(totalsaidao_set, currency, areawords)
    currencyextractrule1 = currency_extract_rule1(totalsaidao_set, areawords)

    for i in range(len(root_clean)):
        try:
            full_text=getpdf(root_clean[i][0])
        except:
            continue
        full_text_split_refine=clean_data(full_text)
        text_first= extract_sentence1(full_text_split_refine,extractsentencerule_track)
        if len(text_first[1])>0:
            continue
        else :
            text_first=extract_sentence1(full_text_split_refine,extractsentencerule_saidao)
        if (len(text_first[0]))==0:
            continue
        else:
            for j in range(len(text_first[0])):
                text_for_process=text_first[0][j]
                sentence=text_first[1][j]
                yanbaoid=root_id[i]

                detail_track_=extract(text_for_process,detailtrackrule)
                saidao_=extract(text_for_process,saidaorule)
                if (len(detail_track_)==0):
                    detail_track_=saidao_.copy()
                black_list=list()
                for k in range(len(blacklist)):
                    black_list+= re.findall(blacklist[k],text_for_process)
                if len(black_list)>0:
                    continue
                else:

                    year_ = extract(text_for_process, extractyearrule)
                    scale_number_ = extract(text_for_process,scalenumberrule )
                    area_ = extract(text_for_process,arearule)
                    units_ = extract(text_for_process,unitsrule)
                    # if len(units):
                    # if units_[0]=='亿':
                    #    scale_number_=scale_number_[0]*100000000
                    # if units_[0]=='万':
                    #    scale_number_=scale_number_[0]*10000
                    currency_ = extract(text_for_process,currencyextractrule )
                    if len(currency_)==0:
                        currency_=extract(text_for_process,currencyextractrule1 )
                    datenow = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

                    add_data = pd.Series(
                        {"行业赛道": saidao_,"细分产品":detail_track_, "年份": year_, "行业规模": scale_number_+units_, "地区": area_, "币种": currency_,"研报ID": yanbaoid, "时间戳": datenow, "研报中位置": sentence, "句子原文": text_for_process})
                    re_df = re_df.append(add_data, ignore_index=True)

    re_df['行业赛道'] = re_df['行业赛道'].str[0]
    re_df['细分产品']=re_df['细分产品'].str[0]
    re_df['年份'] = re_df['年份'].str[0]
    re_df['地区'] = re_df['地区'].str[0]
    re_df['币种'] = re_df['币种'].str[0]
    #re_mid= pd.merge(re_df,re_mid,how='left',left_on=['研报ID','研报中位置'],right_on=['研报id','句子序号'])
    #re_mid=re_mid[['行业赛道','句子原文','研报id','句子序号']]
    re_df = re_df.dropna(axis=0, how='any',subset=['行业赛道','细分产品','行业规模','地区','币种','研报ID','时间戳','研报中位置','句子原文',])
    #re_df = re_df.dropna(axis=0, how='any')
    re_mid= re_df[['行业赛道','细分产品','句子原文','研报ID','研报中位置']]
    re_mid2=re_df[['行业赛道','细分产品','行业规模','币种','地区','年份','研报ID']]
    re_df=re_df.reset_index()
    re_mid = re_mid.reset_index()
    re_mid2 = re_mid2.reset_index()
    for l in range(len(re_df['行业规模'])):
        try:
            temp_scale_number=re.findall('([0-9]+[,\d]*[.\d]*)', str(re_df['行业规模'][l]))[0]
            temp_unit=re.findall('[\u4e00-\u9fa5]', str(re_df['行业规模'][l]))[0]
            temp_str_currency=temp_scale_number + temp_unit + re_df['币种'][l]
            if len(re.findall('万亿', temp_unit)) != 0:
                scale_number_float = float(temp_scale_number) * 1000000000000
            elif len(re.findall('亿', temp_unit)) != 0:
                scale_number_float = float(temp_scale_number) * 100000000
            elif len(re.findall('万', temp_unit)) != 0:
                scale_number_float = float(temp_scale_number) * 10000
            re_mid2['行业规模'][l] = temp_str_currency
            re_df['行业规模'][l] = scale_number_float
        except:
            continue
    track = industry_list['赛道同义词']
    track_posi = list()
    for l in range(len(track)):
        track_posi += [track[l].replace(' ','').replace('，',',').split(',')]
    track_posi = [i for i in track_posi if i != '']
    for m in range(len(re_df['行业赛道'])):
        for n in range(len(track_posi)):
            if re_df['行业赛道'][m] in track_posi[n]:
                re_df['行业赛道'][m]=industry_list['赛道'][n]
            if re_df['细分产品'][m] in track_posi[n]:
                re_df['细分产品'][m]=industry_list['赛道'][n]
    re_mid['行业赛道'] = re_df['行业赛道']
    re_mid2['行业赛道'] = re_df['行业赛道']
    re_mid2=re_mid2.drop('币种',1)
    print("结束: %f " %  time.time())
    return(re_df,re_mid,re_mid2)
