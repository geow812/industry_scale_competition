import os
import logging

# define file path
ROOT_DIR = os.path.abspath('.')
# 统一定义，方便未来可能的改名
INPUT_DIR = 'input'
OUTPUT_DIR = 'output'
DEMO_CSV_PATH = os.path.join(ROOT_DIR, INPUT_DIR, 'demo.csv')
OUTPUT_CSV_PATH = os.path.join(ROOT_DIR, OUTPUT_DIR, 'result.csv')

# define constants
DEMO_NUMBER = 888
DEMO_STR = "This is a demo message"

# define log format
LOG_FILE_NAME = os.path.join(ROOT_DIR, 'log', 'demo_project.log')
LOG_FORMAT = '%(asctime)s %(levelname)s %(funcName)s(%(lineno)d): %(message)s'
loggingConfig = {
    'level': logging.INFO,
    'format': LOG_FORMAT,
    'datefmt': '%Y-%m-%d %H:%M:%S',
    'filename': LOG_FILE_NAME,
    'filemode': 'a'
}
